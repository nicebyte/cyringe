/*! \mainpage Cyringe
\section Introduction
Cyringe will help you automate dependency injection in your C++ project.
TODO: write some docs here with examples.
*/
#pragma once

#include <memory>
#include <atomic>
#include <functional>
#include <unordered_map>
#include <unordered_set>
#include <stdexcept>
#ifndef CYRINGE_DISABLE_RTTI
#include <typeinfo>
#endif // CYRINGE_DISABLE_RTTI
#include <stack>

/// Cyringe is a simple dependency injection library for C++.
namespace cyringe {
namespace detail {

// Auxiliary class for mapping types to arbitrary values.
// Think of this as a hashmap where types act as keys.
template <class ValueType>
class type_map {
  using Container = std::unordered_map<uintptr_t, ValueType>;
public:
  using iterator = typename Container::iterator;
  using const_iterator = typename Container::const_iterator;

  iterator begin() { return container_.begin(); }
  iterator end() { return container_.end(); }
  const_iterator begin() const { return container_.begin(); }
  const_iterator end() const { return container_.end(); }
  const_iterator cbegin() const { return container_.cbegin(); }
  const_iterator cend() const { return container_.cend(); }

  // Find the value in the type map associated with the given type.
  template <class Key>
  iterator find() {
    auto it = container_.find(type_id<Key>());
    return it;
  }

  // Same, const version.
  template <class Key>
  const_iterator find() const {
    auto it = container_.find(type_id<Key>());
    return it;
  }

  // Associate a value with the given type.
  template <class Key>
  void put(ValueType &&value) {
    container_[type_id<Key>()] = std::forward<ValueType>(value);
  }

  // Get a unique integer associated with the given type.
  template <class Key>
  static int type_id() {
    static int id = ++last_type_id_;
    return id;
  }

private:
  static std::atomic<int> last_type_id_;
  Container container_;
};

template <class T>
std::atomic<int> type_map<T>::last_type_id_(0);

// "Instance container" is a hack created to be able to
// hold unique_pointers to instances of different types
// in a single type_map.
class abstract_instance_container {
public:
  virtual ~abstract_instance_container() = default;

  // The type_map holding the object will know the pointer's
  // original type.
  void* get() { return raw_ptr_; }

  abstract_instance_container(abstract_instance_container &&other) {
    *this = std::move(other);
  }

  abstract_instance_container& operator=(abstract_instance_container &&other) {
    raw_ptr_ = other.raw_ptr_;
    other.raw_ptr_ = nullptr;
    return *this;
  }

protected:
  explicit abstract_instance_container(void *raw_ptr) : raw_ptr_(raw_ptr) {}

private:
  void *raw_ptr_;
};

template <class T, class Deleter>
class instance_container : public abstract_instance_container {
public:
  ~instance_container() override = default;

  explicit instance_container(std::unique_ptr<T, Deleter> &&p) :
    abstract_instance_container(p.get()),
    pointer_(std::move(p)) {}

  instance_container(instance_container &&other) {
    *this = std::move(other);
  }

  instance_container& operator=(instance_container &&other) {
    pointer_ = std::move(other);
    abstract_instance_container::operator=(std::move(other));
    return *this;
  }

private:
  std::unique_ptr<T, Deleter> pointer_;
};

template <class T, class Deleter>
std::unique_ptr<abstract_instance_container> wrap_into_instance_container(
  std::unique_ptr<T, Deleter> &&ptr) {
  return std::make_unique<instance_container<T, Deleter>>(std::move(ptr));
}

class UnknownType {};
class injector_builder;
}

/// An instance factory is a function that takes pointers to dependencies as
/// parameters and returns a unique_ptr to the newly created instance.
template <class InstanceType, class Deleter, class ...Deps>
using instance_factory_function_object = std::function<std::unique_ptr<InstanceType, Deleter>(Deps*...)>;

/// An instance factory is a function that takes pointers to dependencies as
/// parameters and returns a unique_ptr to the newly created instance.
template <class InstanceType, class Deleter, class ...Deps>
using instance_factory_function_ptr = std::unique_ptr<InstanceType, Deleter>(*)(Deps*...);

/// Exceptions of this type are thrown when a dependency cannot be provided.
class injection_error : public std::runtime_error {
public:
  explicit injection_error(const std::string &dependency_name,
    const std::string &dependent_name) :
    std::runtime_error(dependency_name + " cannot be provided (required by " + dependent_name + ")") {}
};

/// Provides an interface for injecting dependencies into instance factories.
class injector {
  friend class ::cyringe::detail::injector_builder;
  using instance_map =
    detail::type_map<std::unique_ptr<detail::abstract_instance_container>>;

public:
  /// Move ctor
  injector(injector &&other) { *this = std::move(other); }

  /// Move assignment
  injector& operator=(injector &&other) {
    instance_map_ = std::move(other.instance_map_);
    return *this;
  }

  /// Use this method to get access to an instance stored in this injector.
  /// @return a pointer to an instance in this injector.
  /// @throws injection_error if the injector doesn't have an instance of the given type.
  template <class T, class Dependent = detail::UnknownType>
  T* get_instance() const {
    auto it = instance_map_.find<T>();
    if (it == instance_map_.end()) {
      #ifndef CYRINGE_DISABLE_RTTI
      throw injection_error(typeid(T).name(), typeid(Dependent).name());
      #else
      throw injection_error("Unknown Type", "Unknown Type");
      #endif
    }
    return static_cast<T*>(it->second->get());
  }

  /// Given an instance factory, calls it with its parameters bound to the
  /// instances of corresponding types from this injector.
  /// @parameter instance_factory The instance factory function object
  /// @return A unique_ptr to the instance created by instance_factory using dependencies provided by the injector.
  /// @throws injection_error if some dependencies can't be provided.
  template <class InstanceType, class Deleter, class ...Deps>
  std::unique_ptr<InstanceType, Deleter> inject(
    const instance_factory_function_object<InstanceType, Deleter, Deps...> &instance_factory) const {
    return instance_factory(get_instance<typename std::remove_const<Deps>::type,
      typename std::remove_const<InstanceType>::type>()...);
  }

  /// Similar to inject(const instance_factory_function_object&), but accepts a
  /// pointer to function rather than a std::function as the instance factory.
  /// @parameter instance_factory Pointer to instance factory function
  /// @return A unique_ptr to the instance created by instance_factory using dependencies provided by the injector.
  /// @throws injection_error if some dependencies can't be provided.
  template <class InstanceType, class Deleter, class ...Deps>
  std::unique_ptr<InstanceType, Deleter> inject(
    instance_factory_function_ptr<InstanceType, Deleter, Deps...> instance_factory) const {
    return inject(instance_factory_function_object<InstanceType, Deleter, Deps...>(instance_factory));
  }

  /// Similar to the other inject() functions, but doesn't need an instance factory. It implicitly
  /// creates and uses an instance factory that forwards all of its arguments to InstanceType's ctor.
  /// @return A unique_ptr to the newly created instance
  /// @throws injection_error if some dependencies can't be provided
  template <class InstanceType, class ...Deps>
  std::unique_ptr<InstanceType> inject() const {
    instance_factory_function_object<InstanceType, std::default_delete<InstanceType>, Deps...> instance_factory =
      [](Deps*... params) { return std::make_unique<InstanceType>(params...); };
    return inject(instance_factory);
  }

private:
  injector() = default;
  instance_map instance_map_;
};

namespace detail {
class injector_builder {
public:
  template <class InstanceType, class Deleter, class ...Deps>
  void inject_and_add(
    const instance_factory_function_object<InstanceType, Deleter, Deps...> &instance_factory) {
    auto instance = injector_.inject(instance_factory);
    injector_.instance_map_.put<typename std::remove_const<InstanceType>::type>(
      detail::wrap_into_instance_container(std::move(instance)));
  }

  injector build() {
    return std::move(injector_);
  }

private:
  injector injector_;
};
}

/// Exception of this type is thrown to indicate that there's a cycle
/// somewhere in the dependency graph.
class dependency_graph_cycle_error : public std::runtime_error {
public:
  dependency_graph_cycle_error(const std::string &at) :
    std::runtime_error(std::string("Cycle detected in dependency graph at ") + at) {}
};

/// Provides an interface for creating an injector with all the instances
/// properly initialized in the correct order.
class config {
  template <class ...Args>
  inline void fallthru(Args... args) {}
  using initializer_fn = std::function<void(::cyringe::detail::injector_builder&)>;

public:

  /// Adds an instance factory to the dependency graph.
  /// @parameter instance_factory The instance factory function object
  template <class InstanceType, class Deleter, class ...Deps>
  void add(const instance_factory_function_object<InstanceType, Deleter, Deps...> &instance_factory) {
    int instance_type_id =
      detail::type_map<node_info>::type_id<typename std::remove_const<InstanceType>::type>();
    node_info &new_node_info = graph_[instance_type_id];
    new_node_info.initializer_ =
      [instance_factory](detail::injector_builder &inj) {
      inj.inject_and_add(instance_factory);
    };
    new_node_info.has_iniliatizer_ = true;
    #ifndef CYRINGE_DISABLE_RTTI
    new_node_info.debug_type_name_ = typeid(typename std::remove_const<InstanceType>::type).name();
    #else
    new_node_info.debug_type_name_ = "Unknown Type";
    #endif
    fallthru(
      graph_[detail::type_map<node_info>::type_id<typename std::remove_const<Deps>::type>()]
        .dependents_
        .insert(instance_type_id)...);
  }

  /// Same as add(const instance_factory_function_object&), but accepts a function pointer.
  /// @parameter instance_factory Pointer to an instance factory function
  template <class InstanceType, class Deleter, class ...Deps>
  void add(instance_factory_function_ptr<InstanceType, Deleter, Deps...> instance_factory_ptr) {
    add(instance_factory_function_object<InstanceType, Deleter, Deps...>(instance_factory_ptr));
  }

  /// A convenience function for adding instance factories. The caller only needs to specify
  /// the instance type and the types of dependencies in the template arguments.
  /// The instance type must provide a constructor that accepts pointers to dependencies as
  /// parameters. The instance factory added by this function will use that constructor to
  /// create the new instance. std::default_delete will be used as the deleter for the instance.
  template <class InstanceType, class ...Deps>
  void add() {
    instance_factory_function_object<InstanceType, std::default_delete<InstanceType>, Deps...> instance_factory =
      [](Deps*... params) { return std::make_unique<InstanceType>(params...); };
    add(instance_factory);
  }

  /// Creates all the instances using the provided instance factories in the correct order 
  /// and returns an injector object with the given instances.
  /// @return An injector populated with the new instances. The instances can be retrieved from the injector using
  ///         injector::get() or injected into another instance factory using injector::inject()
  /// @throws dependency_graph_cycle_error if there's a cycle in the dependency graph
  /// @throws injection_error if there is a dependency that cannot be satisfied
  injector build_injector() {
    detail::injector_builder inj;
    std::stack<initializer_fn*> initializers;

    std::unordered_set<int> unmarked_nodes;
    for (auto &node : graph_) {
      node.second.mark_ = node_info::mark::UNMARKED;
      unmarked_nodes.insert(node.first);
    }
    while (!unmarked_nodes.empty()) {
      int node_id = *unmarked_nodes.begin();
      toposort_visit_node(node_id, &unmarked_nodes, &initializers);
    }

    while (!initializers.empty()) {
      (*initializers.top())(inj);
      initializers.pop();
    }
    return std::move(inj.build());
  }

private:
  void toposort_visit_node(
    int node_id,
    std::unordered_set<int> *unmarked_nodes,
    std::stack <initializer_fn*> *output) {
    node_info &info = graph_[node_id];
    if (info.mark_ == node_info::mark::TEMP) {
      throw dependency_graph_cycle_error(info.debug_type_name_);
    }
    else if (info.mark_ == node_info::mark::UNMARKED) {
      unmarked_nodes->erase(node_id);
      info.mark_ = node_info::mark::TEMP;
      for (int dependent : info.dependents_) {
        toposort_visit_node(dependent, unmarked_nodes, output);
      }
      info.mark_ = node_info::mark::PERM;
      if (info.has_iniliatizer_) {
        // if has_initializer_ is false, it means someone depends on this
        // node, but an instance factory for this node has not been provided.
        // This will result in an injection error later.
        output->push(&info.initializer_);
      }
    }
  }

  struct node_info {
    enum class mark {
      UNMARKED, TEMP, PERM
    };
    std::string debug_type_name_;
    initializer_fn initializer_;
    bool has_iniliatizer_ = false;
    std::unordered_set<int> dependents_;
    mark mark_;
  };

  std::unordered_map<int, node_info> graph_;
};
}
