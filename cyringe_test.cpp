#define CATCH_CONFIG_MAIN
#include "cyringe.hpp"
#include "catch.hpp"

/**
       C
       ^
       |
       |
  A--->B<---D
 
**/
struct A {
  A(int val) : a_value(val){}
  A() : a_value(0xa1337) {}
  int a_value;
 };
struct D {
  D(int val) : d_value(val) {}
  D() : d_value(0xd1337) {}
  int d_value;
};
std::unique_ptr<A> a_factory() { return std::make_unique<A>(0xaf00d); }
std::unique_ptr<D> d_factory() { return std::make_unique<D>(0xdf00d); }
struct B {
  B(const A *a, const D *d, int b_value = 0xb1337) :
    b_value_from_d(d->d_value), b_value_from_a(a->a_value), b_value(b_value) {}
  int b_value_from_d;
  int b_value_from_a;
  int b_value;
};
std::unique_ptr<B> b_factory(const A *a, const D *d) { return std::make_unique<B>(a, d, 0xbf00d); }
struct C {
  explicit C(const B *b) : c_value_from_b(b->b_value) {}
  int c_value_from_b;
};
std::unique_ptr<C> c_factory(const B *b) { return std::make_unique<C>(b); }
struct E {
  int e_value_from_a;
  int e_value_from_b;
  int e_value_from_d;
};
std::unique_ptr<E> e_factory(const A *a, const D *d, const B *b) {
  auto e = std::make_unique<E>();
  e->e_value_from_a = a->a_value;
  e->e_value_from_b = b->b_value;
  e->e_value_from_d = d->d_value;
  return std::move(e);
}
TEST_CASE("Simple Dependency Graph", "[simple_graph]") {
  cyringe::config di_config;
  di_config.add(a_factory);
  di_config.add(b_factory);
  di_config.add(c_factory);
  di_config.add(d_factory);
  cyringe::injector inj = di_config.build_injector();
  REQUIRE(inj.get_instance<A>()->a_value ==  0xaf00d);
  REQUIRE(inj.get_instance<B>()->b_value == 0xbf00d);
  REQUIRE(inj.get_instance<D>()->d_value == 0xdf00d);
  REQUIRE(inj.get_instance<B>()->b_value_from_a == 0xaf00d);
  REQUIRE(inj.get_instance<B>()->b_value_from_d == 0xdf00d);
  REQUIRE(inj.get_instance<C>()->c_value_from_b == 0xbf00d);
  auto e = inj.inject(e_factory);
  REQUIRE(e->e_value_from_a == 0xaf00d);
  REQUIRE(e->e_value_from_b == 0xbf00d);
  REQUIRE(e->e_value_from_d == 0xdf00d);
}
TEST_CASE("Simple Dependency Graph - convenience add()", "[simple_graph_conv_add]") {
  cyringe::config di_config;
  di_config.add<A>();
  di_config.add<B, A, D>();
  di_config.add<C, B>();
  di_config.add<D>();
  cyringe::injector inj = di_config.build_injector();
  REQUIRE(inj.get_instance<A>()->a_value == 0xa1337);
  REQUIRE(inj.get_instance<B>()->b_value == 0xb1337);
  REQUIRE(inj.get_instance<D>()->d_value == 0xd1337);
  REQUIRE(inj.get_instance<B>()->b_value_from_a == 0xa1337);
  REQUIRE(inj.get_instance<B>()->b_value_from_d == 0xd1337);
  REQUIRE(inj.get_instance<C>()->c_value_from_b == 0xb1337);
  auto e = inj.inject(e_factory);
  REQUIRE(e->e_value_from_a == 0xa1337);
  REQUIRE(e->e_value_from_b == 0xb1337);
  REQUIRE(e->e_value_from_d == 0xd1337);
}

/*

    F     I
   / \    |
  |   |   |
  v   v   v
  G   H   J
*/
struct F{};
struct G {
  G() : f(nullptr){}
  G(F *f) : f(f){}
  F *f;
};
struct H {
  H() : f(nullptr) {}
  H(F *f) : f(f) {}
  F *f;
};
struct I{};
struct J {
  J() : i(nullptr) {}
  J(I *i) : i(i) {}
  I *i;
};
struct K {
  K(F *f, G *g, I *i) : f(f), g(g), i(i) {}
  F *f;
  G *g;
  I *i;
};
TEST_CASE("Dependency Graph With Multiple Connected Components", "[multi_component_graph]") {
  cyringe::config di_config;
  di_config.add<F>();
  di_config.add<G, F>();
  di_config.add<H, F>();
  di_config.add<I>();
  di_config.add<J, I>();
  cyringe::injector inj = di_config.build_injector();
  REQUIRE(inj.get_instance<F>() != nullptr);
  REQUIRE(inj.get_instance<G>() != nullptr);
  REQUIRE(inj.get_instance<G>()->f == inj.get_instance<F>());
  REQUIRE(inj.get_instance<H>() != nullptr);
  REQUIRE(inj.get_instance<H>()->f == inj.get_instance<F>());
  REQUIRE(inj.get_instance<I>() != nullptr);
  REQUIRE(inj.get_instance<J>() != nullptr);
  REQUIRE(inj.get_instance<J>()->i == inj.get_instance<I>());
  auto k = inj.inject<K, F, G, I>();
  REQUIRE(k->f ==inj.get_instance<F>());
  REQUIRE(k->g == inj.get_instance<G>());
  REQUIRE(k->i == inj.get_instance<I>());
}

/*
  L --> M
  ^     ^
  |     |
   \   /
     N
*/
struct N{};
struct L { L(N *n) : n(n){} N *n;};
struct M { M(L *l, N *n) : l(l), n(n) {} L *l; N *n; };
TEST_CASE("3 Vertices No Cycle", "[3v_no_cycle]") {
  cyringe::config di_config;
  di_config.add<L, N>();
  di_config.add<M, L, N>();
  di_config.add<N>();
  cyringe::injector inj = di_config.build_injector();
  REQUIRE(inj.get_instance<N>() != nullptr);
  REQUIRE(inj.get_instance<L>() != nullptr);
  REQUIRE(inj.get_instance<L>()->n == inj.get_instance<N>());
  REQUIRE(inj.get_instance<M>() != nullptr);
  REQUIRE(inj.get_instance<M>()->n == inj.get_instance<N>());
  REQUIRE(inj.get_instance<M>()->l == inj.get_instance<L>());
}
TEST_CASE("Missing dependency", "[missing_dependency]") {
  cyringe::config di_config;
  di_config.add<M, L, N>();
  di_config.add<N>();
  REQUIRE_THROWS_AS(di_config.build_injector(), cyringe::injection_error);
}
struct P;
struct O {
  O(P*){}
};
struct P {
  P(O*){}
};
TEST_CASE("Simple Cycle", "[simple_cycle]") {
  cyringe::config di_config;
  di_config.add<P, O>();
  di_config.add<O, P>();
  REQUIRE_THROWS_AS(di_config.build_injector(), cyringe::dependency_graph_cycle_error);
}
struct S;
struct Q {
  Q(S*){}
};
struct R {
  R(Q*){}
};
struct S {
  S(R*){}
};
TEST_CASE("3 Vertices Cycle", "[3v_cycle]") {
  cyringe::config di_config;
  di_config.add<Q, S>();
  di_config.add<R, Q>();
  di_config.add<S, R>();
  REQUIRE_THROWS_AS(di_config.build_injector(), cyringe::dependency_graph_cycle_error);
}
TEST_CASE("Missing dependency 2", "[missing_dep_2]") {
  cyringe::config di_config;
  di_config.add<L, N>();
  di_config.add<M, L, N>();
  di_config.add<N>();
  cyringe::injector inj = di_config.build_injector();
  REQUIRE(inj.get_instance<N>() != nullptr);
  REQUIRE(inj.get_instance<L>() != nullptr);
  REQUIRE(inj.get_instance<L>()->n == inj.get_instance<N>());
  REQUIRE(inj.get_instance<M>() != nullptr);
  REQUIRE(inj.get_instance<M>()->n == inj.get_instance<N>());
  REQUIRE(inj.get_instance<M>()->l == inj.get_instance<L>());
  REQUIRE_THROWS_AS((inj.inject<Q,S>()), cyringe::injection_error);
}
